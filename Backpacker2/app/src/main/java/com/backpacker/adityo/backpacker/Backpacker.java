package com.backpacker.adityo.backpacker;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.backpacker.adityo.backpacker.adapter.FeedAdapter;
import com.backpacker.adityo.backpacker.adapter.ImageAdapter;
import com.backpacker.adityo.backpacker.customclass.Destinasi;
import com.backpacker.adityo.backpacker.customclass.Friends;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class Backpacker extends FragmentActivity implements ActionBar.TabListener {
    AppSectionsPagerAdapter mAppSectionsPagerAdapter;

    public static ArrayList<Destinasi> dest = new ArrayList<>();
    public static ArrayList<Destinasi> posts = new ArrayList<>();

    public static ArrayList<Friends> friends = new ArrayList<>();
    private static ArrayList<Marker> markers = new ArrayList<>();

    private static String spUsername;
    android.support.v4.view.ViewPager mViewPager;
    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);

        mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(getSupportFragmentManager());
        sp = getSharedPreferences(Login.MyPREFERENCES, Context.MODE_PRIVATE);
        spUsername = sp.getString(Login.name,"");

        final ActionBar actionBar = getActionBar();

        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.txtTitle);

        ImageButton imageButton = (ImageButton) mCustomView
                .findViewById(R.id.imageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                logout();
            }
        });

        actionBar.setCustomView(mCustomView);
        actionBar.setDisplayShowCustomEnabled(true);
        if (actionBar != null) {
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        }

        mViewPager = (android.support.v4.view.ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mAppSectionsPagerAdapter);

        mViewPager.setOnPageChangeListener(new android.support.v4.view.ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // When swiping between different app sections, select the corresponding tab.
                // We can also use ActionBar.Tab#select() to do this if we have a reference to the
                // Tab.
                if (actionBar != null) {
                    actionBar.setSelectedNavigationItem(position);
                }
            }
        });

      /*  for (int i = 0; i < mAppSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by the adapter.
            // Also specify this Activity object, which implements the TabListener interface, as the
            // listener for when this tab is selected.

            if (actionBar != null) {
                actionBar.addTab(
                        actionBar.newTab()
                                .setText(mAppSectionsPagerAdapter.getPageTitle(i))
                                .setTabListener(this));
            }

        }*/

        if (actionBar != null) {
            actionBar.addTab(
                    actionBar.newTab()
                            .setIcon(R.mipmap.home)
                            .setTabListener(this));

            actionBar.addTab(
                    actionBar.newTab()
                            .setIcon(R.mipmap.search)
                            .setTabListener(this));

            actionBar.addTab(
                    actionBar.newTab()
                            .setIcon(R.mipmap.notif)
                            .setTabListener(this));

            actionBar.addTab(
                    actionBar.newTab()
                            .setIcon(R.mipmap.profile)
                            .setTabListener(this));
        }

    }

    private void logout(){
        AlertDialog.Builder builder = new AlertDialog.Builder(Backpacker.this);
        builder.setCancelable(false);
        builder.setMessage("Sure to logout Backpacker?");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application
                LogoutTask task = new LogoutTask();
                task.execute();
            }
        });
        builder.setNegativeButton("No",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert=builder.create();
        alert.show();
    }

    /***********************************LOGOUT APLIKASI************************************************************************/
    private class LogoutTask extends AsyncTask<String, Void, String> {
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Backpacker.this);
            pDialog.setMessage("Sign Out...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String response = null;
            String res = null;
            try {
                response = CustomHttpClient.executeHttpLogout();
                res=response.toString();
                res= res.replaceAll("\\s+","");
            }
            catch (Exception e) {
                //txt_Error.setText(e.toString());
            }
            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            SharedPreferences sharedpreferences = getSharedPreferences(Login.MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.clear();
            editor.commit();

            Intent in = new Intent(Backpacker.this, Login.class);
            startActivity(in);
            finish();
        }//close onPostExecute
    }// close validateUserTask

    @Override
    public void onBackPressed(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Sure to exit Backpacker?");

        builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert=builder.create();
        alert.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_pager, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    /*-----------------------------------------------------------------------------------------------*/
    public static class AppSectionsPagerAdapter extends FragmentPagerAdapter {

        public AppSectionsPagerAdapter(FragmentManager fm) {
            super(fm);

        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    // The first section of the app is the most interesting -- it offers
                    // a launchpad into the other demonstrations in this example application.
                    return new HomeFragment();

                case 1:
                    // The other sections of the app are dummy placeholders.
                    return new TraceFragment();

                case 2:
                    return new NotifFragment();
                default:
                    return new ProfileFragment();

            }
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "Section " + (position + 1);
        }
    }


    public static class HomeFragment extends Fragment {
        ImageButton home,search,post,notif,profile, buttTitle;
        TextView textTitle;
        Uri fileUri;
        public static final int media_foto = 10;
        SwipeRefreshLayout mSwipeRefreshLayout;
        RecyclerView rvFeed;
        FeedAdapter feedAdapter;
        RecyclerView.LayoutManager layoutManager;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.activity_home, container, false);
            //textTitle = (TextView)rootView.findViewById(R.id.textTitle);
            home = (ImageButton)rootView.findViewById(R.id.homeButton);
            search = (ImageButton)rootView.findViewById(R.id.searchButton);
            post = (ImageButton)rootView.findViewById(R.id.btnCreate);
            notif = (ImageButton)rootView.findViewById(R.id.notifButton);
            profile = (ImageButton)rootView.findViewById(R.id.profileButton);
            //buttTitle = (ImageButton)rootView.findViewById(R.id.buttTitle);
            rvFeed = (RecyclerView)rootView.findViewById(R.id.rvFeed);
            mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.activity_main_swipe_refresh_layout);

            layoutManager = new LinearLayoutManager(rootView.getContext());
            rvFeed.setLayoutManager(layoutManager);


            //textTitle.setText(sp.getString(Login.name,"username"));

            
             UpdateContentTask updateContent = new UpdateContentTask();
             updateContent.execute();



            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    UpdateContentTask updateContent = new UpdateContentTask();
                    updateContent.execute();
                }
            });


            post.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
               /* Intent in = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivity(in);*/
                    String[] tes = {"Take Picture","From Galery"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(rootView.getContext());
                    builder.setTitle("MEH NGEPOST CUK???")
                            .setItems(tes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if(which==0){
                                        Intent in = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                        fileUri = Uri.fromFile(getOutputMediaFile(media_foto));
                                        in.putExtra(MediaStore.EXTRA_OUTPUT,fileUri);
                                        startActivityForResult(in, 0);
                                    }else {
                                        Intent pick = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                        startActivityForResult(pick,1);
                                    }
                                }
                            });

                    AlertDialog alert=builder.create();
                    alert.show();
                }
            });



            return rootView;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            ShowContentTask show = new ShowContentTask();
            show.execute();
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            if(requestCode == 0){
                if (resultCode == RESULT_OK) {
                    Intent in = new Intent(getActivity(),posting.class);
                    in.putExtra("uri",fileUri.getPath());
                    startActivity(in);
                }
            }else if(requestCode == 1) {
                if (resultCode == RESULT_OK) {
                    Uri select = data.getData();
                    Intent in = new Intent(getActivity(),posting.class);
                    in.putExtra("uriGaleri",select.toString());
                    startActivity(in);
                }
            }
        }

        private File getOutputMediaFile(int type){
            File med = new File(Environment.getExternalStorageDirectory()+"/Backpacker");
            med.mkdirs();
            String time = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
            File medfile;
            if(type == media_foto){
                medfile = new File(med.getPath() + File.separator + "IMG" + time + ".jpg");
            } else {
                return null;
            }
            return medfile;
        }

        private void setupFeed(ArrayList<Destinasi> list) {
            feedAdapter = new FeedAdapter(getActivity(),rvFeed,list);
            rvFeed.setAdapter(feedAdapter);
        }


        /***********************************NAMPILIN POSTINGAN************************************************************************/
        private class ShowContentTask extends AsyncTask<String, Void, ArrayList<Destinasi>> {
            private ProgressDialog pDialog;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pDialog = new ProgressDialog(getActivity());
                pDialog.setMessage("ENTENI WAE CUK!!!");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(true);
                pDialog.show();
            }

            @Override
            protected ArrayList<Destinasi> doInBackground(String... params) {
                ArrayList<Destinasi> respon = new ArrayList<>();
                try {
                    respon = CustomHttpClient.executeHttpGet("http://progmob.esy.es/getUsers.php",spUsername);
                }
                catch (Exception e) {
                    //txt_Error.setText(e.toString());
                }
                return  respon;
            }//close doInBackground

            @Override
            protected void onPostExecute(ArrayList<Destinasi> result) {
                pDialog.dismiss();
                dest = result;
                setupFeed(result);

                mSwipeRefreshLayout.setRefreshing(false);
            }//close onPostExecute
        }


        private class UpdateContentTask extends AsyncTask<String, Void, ArrayList<Destinasi>> {
            @Override
            protected ArrayList<Destinasi> doInBackground(String... params) {
                ArrayList<Destinasi> respon = new ArrayList<>();
                try {
                    respon = CustomHttpClient.executeHttpGet("http://progmob.esy.es/getUsers.php",spUsername);
                } catch (Exception e) {
                    //txt_Error.setText(e.toString());
                }
                return respon;
            }//close doInBackground

            @Override
            protected void onPostExecute(ArrayList<Destinasi> result) {

                dest = result;
                setupFeed(result);

                mSwipeRefreshLayout.setRefreshing(false);
            }//close onPostExecute
        }
    }

    public static class TraceFragment extends Fragment {
        private SupportMapFragment fragment;
        private GoogleMap map;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.trace_friends, container, false);

            return rootView;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            FragmentManager fm = getChildFragmentManager();
            fragment = (SupportMapFragment) fm.findFragmentById(R.id.map_container);
            if (fragment == null) {
                fragment = SupportMapFragment.newInstance();
                fm.beginTransaction().replace(R.id.map_container, fragment).commit();
            }

        }

        @Override
        public void onResume() {
            super.onResume();
            new TraceFriends().execute();
        }

        private class TraceFriends extends AsyncTask<String, Void, ArrayList<Friends>> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected ArrayList<Friends> doInBackground(String... params) {
                ArrayList<Friends> respon = new ArrayList<>();
                try {
                    respon = CustomHttpClient.executeHttpGetFriends("http://progmob.esy.es/trace.php",spUsername);
                }
                catch (Exception e) {
                }
                return  respon;
            }//close doInBackground

            @Override
            protected void onPostExecute(ArrayList<Friends> result) {
                friends.clear();
                markers.clear();
                friends = result;
                map = fragment.getMap();

                for(int i=0;i<friends.size();i++){
                    Friends t = friends.get(i);
                    LatLng latLng = new LatLng(t.getLocLat(),t.getLocLong());
                    MarkerOptions mark = new MarkerOptions().position(latLng).title(""+t.getUsername()).snippet("Location: " + t.getLocation()).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                    Marker m = map.addMarker(mark);
                    markers.add(m);

                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(markers.get(i).getPosition(), 10);
                    map.animateCamera(cameraUpdate);
                }
            }//close onPostExecute
        }


    }

    public static class NotifFragment extends Fragment {

        public static final String ARG_SECTION_NUMBER = "section_number";

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.notification, container, false);
            Bundle args = getArguments();/*
            ((TextView) rootView.findViewById(android.R.id.text1)).setText(
                    getString(R.string.dummy_section_text, args.getInt(ARG_SECTION_NUMBER)));
*/            return rootView;
        }
    }

    public static class ProfileFragment extends Fragment {
        GridView grid;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.activity_profile, container, false);
            grid = (GridView)rootView.findViewById(R.id.gridView);

            new ShowPost().execute();
            return rootView;
        }

        private class ShowPost extends AsyncTask<String, Void, ArrayList<Destinasi>> {
            private ProgressDialog pDialog;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pDialog = new ProgressDialog(getActivity());
                pDialog.setMessage("LODING");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(true);
                pDialog.show();
            }

            @Override
            protected ArrayList<Destinasi> doInBackground(String... params) {
                ArrayList<Destinasi> respon = new ArrayList<>();
                try {
                    respon = CustomHttpClient.executeHttpGet("http://progmob.esy.es/post.php",spUsername);
                }
                catch (Exception e) {
                    //txt_Error.setText(e.toString());
                }
                return  respon;
            }//close doInBackground

            @Override
            protected void onPostExecute(ArrayList<Destinasi> result) {
                pDialog.dismiss();
                posts = result;
                grid.setAdapter(new ImageAdapter(getActivity(),posts));
            }//close onPostExecute
        }
    }
}

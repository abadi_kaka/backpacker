package com.backpacker.adityo.backpacker.customclass;

import android.graphics.Bitmap;
import android.media.Image;

import java.util.Date;

/**
 * Created by adityo on 3/31/2015.
 */
public class Destinasi {
    private Integer id;
    private String username;
    private String loc;
    private double locLat;
    private double locLong;
    private String content;
    private double price;
    private Bitmap img;
    private Date postdate;


    public Destinasi(Integer id, String u,String l,Double loclat,Double loclong,String c,Double p,Bitmap b, Date d){
        this.id = id;
        this.username = u;
        this.loc = l;
        this.locLat = loclat;
        this.locLong = loclong;
        this.content = c;
        this.price = p;
        this.img = b;
        this.postdate = d;

    }

    public Destinasi(Integer id,String l,String c,Double p,Bitmap b){
        this.id = id;
        this.loc = l;
        this.content = c;
        this.price = p;
        this.img = b;
    }

    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Bitmap getImg() {
        return img;
    }

    public void setImg(Bitmap img) {
        this.img = img;
    }

    public Date getPostdate() {
        return postdate;
    }

    public void setPostdate(Date postdate) {
        this.postdate = postdate;
    }

    public double getLocLong() {
        return locLong;
    }

    public void setLocLong(double locLong) {
        this.locLong = locLong;
    }

    public double getLocLat() {
        return locLat;
    }

    public void setLocLat(double locLat) {
        this.locLat = locLat;
    }
}

package com.backpacker.adityo.backpacker;

/**
 * Created by macbook on 2/19/15.
 */
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Base64;

import com.backpacker.adityo.backpacker.customclass.Comments;
import com.backpacker.adityo.backpacker.customclass.Destinasi;
import com.backpacker.adityo.backpacker.customclass.Friends;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class CustomHttpClient {
    /** The time it takes for our client to timeout */
    public static final int HTTP_TIMEOUT = 30 * 1000; // milliseconds

    /** Single instance of our HttpClient */
    private static HttpClient mHttpClient;

    /**
     * Get our single instance of our HttpClient object
     * @return an HttpClient object with connection parameters set
     */
    private static HttpClient getHttpClient() {
        if (mHttpClient == null) {
            mHttpClient = new DefaultHttpClient();
            final HttpParams params = mHttpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, HTTP_TIMEOUT);
            HttpConnectionParams.setSoTimeout(params, HTTP_TIMEOUT);
            ConnManagerParams.setTimeout(params, HTTP_TIMEOUT);
        }
        return mHttpClient;
    }
    /**
     * Performs an HTTP Post request to the specified url with the
     * specified parameters.
     * @param url The web address to post the request to
     * @param postParameters The parameters to send via the request
     * @return The result of the request
     * @throws Exception
     */
    public static String executeHttpPost(String url, ArrayList<NameValuePair> postParameters) throws Exception {
        BufferedReader in = null;
        try {
            HttpClient client = getHttpClient();
            HttpPost request = new HttpPost(url);

            UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
            request.setEntity(formEntity);
            HttpResponse response = client.execute(request);
            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }
            in.close();

            String result = sb.toString();

            return result;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public static String executeHttpMultipartPost(String url, String[] params, Double locLat, Double locLong) throws Exception {
        BufferedReader in = null;

            HttpClient client = getHttpClient();
            HttpPost request = new HttpPost(url);

            BitmapFactory.Options opt = new BitmapFactory.Options();
            opt.inSampleSize = 4;
            Bitmap bitmap = BitmapFactory.decodeFile(params[3],opt);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 50, stream);
            byte[] byteArray = stream.toByteArray();

            File file = new File(params[3]);
            //FileBody fb = new FileBody(file);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

            builder.addTextBody("username", params[0]);
            builder.addTextBody("loc", params[1]);
            builder.addTextBody("price", params[2]);
            builder.addTextBody("locLat",String.valueOf(locLat));
            builder.addTextBody("locLong",String.valueOf(locLong));
            builder.addPart("img", new ByteArrayBody(byteArray, file.getName()));

            HttpEntity yourEntity = builder.build();

            request.setEntity(yourEntity);

            HttpResponse response = client.execute(request);

            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }
            in.close();

            String result = sb.toString();

            //String result = params[3];
            return result;

    }


    /**
     * Performs an HTTP GET request to the specified url.
     *
     * @param url The web address to post the request to
     * @return The result of the request
     * @throws Exception
     */
    public static ArrayList<Destinasi> executeHttpGet(String url,String uname) throws Exception {
        BufferedReader in = null;
        try {
            HttpClient client = getHttpClient();
            //HttpGet request = new HttpGet();
            //request.setURI(new URI(url));

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add( new BasicNameValuePair("username", uname ) );
            URI uri = new URI( url + "?" + URLEncodedUtils.format(params, "utf-8"));

            HttpUriRequest request = new HttpGet(uri);
            request.setHeader("Accept", "application/json, text/javascript, */*; q=0.01");

            HttpResponse response = client.execute(request);
            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            /*
            byte[] b = new byte[1024];
            InputStream tes = response.getEntity().getContent();
            tes.read(b);
            */

            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }
            in.close();
            String temp = sb.toString();

            JSONObject jObject = null;
            try {
                jObject = new JSONObject(temp);

            } catch (Exception e) {
                e.printStackTrace();
            }
             /*
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();
            xpp.setInput(new StringReader(temp));*/


            ArrayList<Destinasi> listResult = new ArrayList<>();
            String loc = null, content = null, username = null;
            Double loclat = null, loclong = null;
            Integer id = null;
            Double price = null;
            Date postdate = null;
            byte[] byteImg = null;

            JSONArray jArray = jObject.getJSONArray("destinasi");

            for (int i = 0; i < jArray.length(); i++) {
                try {
                    JSONObject oneObject = jArray.getJSONObject(i);
                    id = oneObject.getInt("id");
                    username = oneObject.getString("username");
                    loc = oneObject.getString("loc");
                    loclat = oneObject.getDouble("locLat");
                    loclong = oneObject.getDouble("locLong");
                    content = oneObject.getString("content");
                    price = oneObject.getDouble("price");

                    DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    postdate = format.parse(oneObject.getString("postdate"));

                    byteImg = Base64.decode(oneObject.getString("picture"),Base64.DEFAULT);
                    Bitmap bitmap = BitmapFactory.decodeByteArray(byteImg,0,byteImg.length);


                    listResult.add( new Destinasi(id,username,loc,loclat,loclong,content,price,bitmap,postdate));
                } catch (JSONException e) {
                    // Oops
                }
            }
            /*
            int eventType = xpp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if(eventType == XmlPullParser.START_TAG && xpp.getName().equals("destination")){

                    id = Integer.valueOf(xpp.getAttributeValue(0));

                }

                if(eventType == XmlPullParser.START_TAG && xpp.getName().equals("username")){
                    xpp.next();
                    username = xpp.getText();

                }else if(eventType == XmlPullParser.START_TAG && xpp.getName().equals("loc")){
                    xpp.next();
                    loc = xpp.getText();

                }else if(eventType == XmlPullParser.START_TAG && xpp.getName().equals("locLat")){
                    xpp.next();
                    loclat = Double.valueOf(xpp.getText());

                }else if(eventType == XmlPullParser.START_TAG && xpp.getName().equals("locLong")){
                    xpp.next();
                    loclong = Double.valueOf(xpp.getText());

                } else if(eventType == XmlPullParser.START_TAG && xpp.getName().equals("content")){
                    xpp.next();
                    content = xpp.getText();

                } else if(eventType == XmlPullParser.START_TAG && xpp.getName().equals("price")){
                    xpp.next();
                    price = Double.valueOf(xpp.getText());

                } else if(eventType == XmlPullParser.START_TAG && xpp.getName().equals("picture")){
                    xpp.next();
                    byteImg = Base64.decode(xpp.getText(),Base64.DEFAULT);

                } else if(eventType == XmlPullParser.START_TAG && xpp.getName().equals("postdate")){
                    xpp.next();
                    DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    postdate = format.parse(xpp.getText());

                }

                if(id != null && loc != null && content != null && price != null && byteImg != null && username != null && postdate != null && loclat !=null && loclong != null){
                    Bitmap bitmap = BitmapFactory.decodeByteArray(byteImg,0,byteImg.length);
                    listResult.add( new Destinasi(id,username,loc,loclat,loclong,content,price,bitmap,postdate));

                    username = null; loc = null; price = null; content = null; id=null; byteImg = null; postdate = null;
                    loclat = null; loclong = null;
                }
                eventType = xpp.next();
            }*/


            return listResult;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static ArrayList<Friends> executeHttpGetFriends(String url,String uname) throws Exception {
        BufferedReader in = null;
        try {
            HttpClient client = getHttpClient();

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add( new BasicNameValuePair("username", uname ) );
            URI uri = new URI( url + "?" + URLEncodedUtils.format(params, "utf-8"));

            HttpUriRequest request = new HttpGet(uri);
            request.setHeader("Accept", "application/json, text/javascript, */*; q=0.01");

            HttpResponse response = client.execute(request);
            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }
            in.close();
            String temp = sb.toString();

            JSONObject jObject = null;
            try {
                jObject = new JSONObject(temp);

            } catch (Exception e) {
                e.printStackTrace();
            }
            ArrayList<Friends> listResult = new ArrayList<>();
            String loc = null, username = null;
            Double loclat = null, loclong = null;
            Date date = null;

            JSONArray jArray = jObject.getJSONArray("friends");

            for (int i = 0; i < jArray.length(); i++) {
                try {
                    JSONObject oneObject = jArray.getJSONObject(i);
                    username = oneObject.getString("username");
                    loc = oneObject.getString("loc");
                    loclat = oneObject.getDouble("locLat");
                    loclong = oneObject.getDouble("locLong");

                    DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    date = format.parse(oneObject.getString("date"));


                    listResult.add( new Friends(username,loc,loclat,loclong,date));
                } catch (JSONException e) {
                    // Oops
                }
            }

            return listResult;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static ArrayList<Comments> executeHttpGetComments(String url,String id) throws Exception {
        BufferedReader in = null;
        try {
            HttpClient client = getHttpClient();

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add( new BasicNameValuePair("id", id) );
            URI uri = new URI( url + "?" + URLEncodedUtils.format(params, "utf-8"));

            HttpUriRequest request = new HttpGet(uri);
            request.setHeader("Accept", "application/json, text/javascript, */*; q=0.01");

            HttpResponse response = client.execute(request);
            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }
            in.close();
            String temp = sb.toString();

            JSONObject jObject = null;
            try {
                jObject = new JSONObject(temp);

            } catch (Exception e) {
                e.printStackTrace();
            }
            ArrayList<Comments> listResult = new ArrayList<>();
            String content = null, username = null;
            int com_id, dest_id;
            Date date = null;

            JSONArray jArray = jObject.getJSONArray("comments");

            for (int i = 0; i < jArray.length(); i++) {
                try {
                    JSONObject oneObject = jArray.getJSONObject(i);
                    username = oneObject.getString("username");
                    com_id = oneObject.getInt("com_id");
                    dest_id = oneObject.getInt("dest_id");
                    content = oneObject.getString("content");

                    DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    date = format.parse(oneObject.getString("com_time"));


                    listResult.add( new Comments(com_id,dest_id,username,content,date));
                } catch (JSONException e) {
                    // Oops
                }
            }

            return listResult;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }



    //For LOGOUT
    public static String executeHttpLogout() throws Exception {
        BufferedReader in = null;
        try {
            HttpClient client = getHttpClient();
            HttpGet request = new HttpGet();
            request.setURI(new URI("http://progmob.esy.es/logout.php"));
            HttpResponse response = client.execute(request);
            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }
            in.close();

            String result = sb.toString();
            return result;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
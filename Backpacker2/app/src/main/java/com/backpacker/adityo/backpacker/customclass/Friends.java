package com.backpacker.adityo.backpacker.customclass;

import java.util.Date;

/**
 * Created by adityo on 5/12/2015.
 */
public class Friends {
    String username;
    String location;
    Double locLat;
    Double locLong;
    Date date;

    public Friends(String username, String location, Double locLat, Double locLong, Date date) {
        this.username = username;
        this.location = location;
        this.locLat = locLat;
        this.locLong = locLong;
        this.date = date;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Double getLocLat() {
        return locLat;
    }

    public void setLocLat(Double locLat) {
        this.locLat = locLat;
    }

    public Double getLocLong() {
        return locLong;
    }

    public void setLocLong(Double locLong) {
        this.locLong = locLong;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}

package com.backpacker.adityo.backpacker.customclass;

import android.graphics.Bitmap;

import java.util.Date;

/**
 * Created by adityo on 5/1/2015.
 */
public class Comments {
    Bitmap profCom = null;
    Integer com_id;
    Integer dest_id;
    String username;
    String content;
    Date com_time;


    public Comments(Integer com_id, Integer dest_id, String username, String content, Date com_time) {
        this.com_id = com_id;
        this.dest_id = dest_id;
        this.username = username;
        this.content = content;
        this.com_time = com_time;
    }

    public Bitmap getProfCom() {
        return profCom;
    }

    public void setProfCom(Bitmap profCom) {
        this.profCom = profCom;
    }

    public Integer getCom_id() {
        return com_id;
    }

    public void setCom_id(Integer com_id) {
        this.com_id = com_id;
    }

    public Integer getDest_id() {
        return dest_id;
    }

    public void setDest_id(Integer dest_id) {
        this.dest_id = dest_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCom_time() {
        return com_time;
    }

    public void setCom_time(Date com_time) {
        this.com_time = com_time;
    }
}

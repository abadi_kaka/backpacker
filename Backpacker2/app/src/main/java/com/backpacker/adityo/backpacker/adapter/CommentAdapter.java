package com.backpacker.adityo.backpacker.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.backpacker.adityo.backpacker.customclass.Comments;
import com.backpacker.adityo.backpacker.R;

import java.util.ArrayList;

/**
 * Created by adityo on 5/1/2015.
 */
public class CommentAdapter extends ArrayAdapter<Comments> {

    public CommentAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public CommentAdapter(Context context, int resource, ArrayList<Comments> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {

            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.comment_adapter, null);

        }

        Comments p = getItem(position);

        if (p != null) {

            ImageView profImg = (ImageView) v.findViewById(R.id.profpict);
            TextView uname = (TextView) v.findViewById(R.id.txtUname);
            TextView com = (TextView)v.findViewById(R.id.txtCom);

            if(profImg != null) {
                //profImg.setImageBitmap(p.getProfCom());
            }

            if(uname != null){
                uname.setText(p.getUsername());
            }

            if(com != null){
                com.setText(p.getContent());
            }


        }

        return v;

    }
}

package com.backpacker.adityo.backpacker.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.backpacker.adityo.backpacker.Maps;
import com.backpacker.adityo.backpacker.customclass.Destinasi;
import com.backpacker.adityo.backpacker.R;
import com.backpacker.adityo.backpacker.ShowPost;

import java.util.ArrayList;

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.ViewHolder>{

    private ArrayList<Destinasi> dest = new ArrayList<>();
    private Context context;
    private RecyclerView r;
   /* private OnFeedItemClickListener onFeedItemClickListener;*/

    // Provide a suitable constructor (depends on the kind of dataset)
    public FeedAdapter(Context con, RecyclerView rec, ArrayList<Destinasi> list) {
        this.context = con;
        this.dest = list;
        this.r = rec;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        public TextView loc, user;
        public ImageView img;
        public ViewHolder(View v) {
            super(v);
            loc = (TextView) v.findViewById(R.id.txtLoc);
            img = (ImageView) v.findViewById(R.id.contentImage);
            user = (TextView) v.findViewById(R.id.txtUname);
            loc.setOnClickListener(this);
            img.setOnClickListener(this);
         }

        @Override
        public void onClick(View v) {
            int viewId = v.getId();
            if(viewId == R.id.contentImage){
                Intent in = new Intent(v.getContext(), ShowPost.class);
                in.putExtra("position",getLayoutPosition());
                v.getContext().startActivity(in);
            } else if(viewId == R.id.txtLoc){
                Intent in = new Intent(v.getContext(), Maps.class);
                in.putExtra("position",getLayoutPosition());
                v.getContext().startActivity(in);
            }
        }
    }



    // Create new views (invoked by the layout manager)
    @Override
    public FeedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_feed, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
       /* vh.loc.setOnClickListener(this);
        vh.img.setOnClickListener(this);*/
        return vh;
    }

  /*  public void setOnFeedItemClickListener(OnFeedItemClickListener onFeedItemClickListener) {
        this.onFeedItemClickListener = onFeedItemClickListener;
    }*/


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Destinasi d = dest.get(position);
        holder.loc.setText(d.getLoc());
        holder.img.setImageBitmap(d.getImg());
        holder.user.setText(d.getUsername());

    }





    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return dest.size();
    }


  /*  public interface OnFeedItemClickListener {
        public void onPictureClick(View v, int position);
    }*/
}

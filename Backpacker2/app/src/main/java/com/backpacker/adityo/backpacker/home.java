package com.backpacker.adityo.backpacker;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.backpacker.adityo.backpacker.adapter.FeedAdapter;
import com.backpacker.adityo.backpacker.customclass.Destinasi;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;



public class home extends Fragment {
    ImageButton home,search,post,notif,profile, buttTitle;
    TextView textTitle;

    Uri fileUri;
    public static final int media_foto = 10;

    SwipeRefreshLayout mSwipeRefreshLayout;
    public static ArrayList<Destinasi> dest = new ArrayList<>();
    private static String spUsername;
    RecyclerView rvFeed;
    FeedAdapter feedAdapter;
    RecyclerView.LayoutManager layoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.activity_home, container, false);
        textTitle = (TextView)rootView.findViewById(R.id.textTitle);
        home = (ImageButton)rootView.findViewById(R.id.homeButton);
        search = (ImageButton)rootView.findViewById(R.id.searchButton);
        post = (ImageButton)rootView.findViewById(R.id.btnCreate);
        notif = (ImageButton)rootView.findViewById(R.id.notifButton);
        profile = (ImageButton)rootView.findViewById(R.id.profileButton);
        buttTitle = (ImageButton)rootView.findViewById(R.id.buttTitle);
        rvFeed = (RecyclerView)rootView.findViewById(R.id.rvFeed);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.activity_main_swipe_refresh_layout);

        layoutManager = new LinearLayoutManager(rootView.getContext());
        rvFeed.setLayoutManager(layoutManager);

        SharedPreferences sp = rootView.getContext().getSharedPreferences(Login.MyPREFERENCES, Context.MODE_PRIVATE);
        textTitle.setText(sp.getString(Login.name,"username"));
        spUsername = sp.getString(Login.name,"username");

        ShowContentTask show = new ShowContentTask();
        show.execute();



        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                UpdateContentTask updateContent = new UpdateContentTask();
                updateContent.execute();
            }
        });


        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(rootView.getContext(),ProfileActivity.class);
                startActivity(in);
            }
        });

        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent in = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivity(in);*/
                String[] tes = {"Take Picture","From Galery"};
                AlertDialog.Builder builder = new AlertDialog.Builder(rootView.getContext());
                builder.setTitle("MEH NGEPOST CUK???")
                        .setItems(tes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if(which==0){
                                    Intent in = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    fileUri = Uri.fromFile(getOutputMediaFile(media_foto));
                                    in.putExtra(MediaStore.EXTRA_OUTPUT,fileUri);
                                    startActivityForResult(in, 0);
                                }else {
                                    Intent pick = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    startActivityForResult(pick,1);
                                }
                            }
                        });

                AlertDialog alert=builder.create();
                alert.show();
            }
        });

        buttTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setCancelable(false);
                builder.setMessage("Sure to logout Backpacker?");

                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //if user pressed "yes", then he is allowed to exit from application
                        LogoutTask task = new LogoutTask();
                        task.execute();
                    }
                });
                builder.setNegativeButton("No",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //if user select "No", just cancel this dialog and continue with app
                        dialog.cancel();
                    }
                });
                AlertDialog alert=builder.create();
                alert.show();
            }
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 0){
            if (resultCode == 1) {
                Intent in = new Intent(getActivity(),posting.class);
                in.putExtra("uri",fileUri.getPath());
                startActivity(in);
            }
        }else if(requestCode == 1) {
            if (resultCode == 1) {
                Uri select = data.getData();
                Intent in = new Intent(getActivity(),posting.class);
                in.putExtra("uriGaleri",select.toString());
                startActivity(in);
            }
        }
    }


    /*@Override
    public void onBackPressed(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Sure to exit Backpacker?");

        builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert=builder.create();
        alert.show();
    }*/



    private File getOutputMediaFile(int type){
        File med = new File(Environment.getExternalStorageDirectory()+"/Backpacker");
        med.mkdirs();
        String time = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File medfile;
        if(type == media_foto){
            medfile = new File(med.getPath() + File.separator + "IMG" + time + ".jpg");
        } else {
            return null;
        }
        return medfile;
    }



    private void setupFeed(ArrayList<Destinasi> list) {
        feedAdapter = new FeedAdapter(getActivity(),rvFeed,list);
        rvFeed.setAdapter(feedAdapter);
    }

    /***********************************LOGOUT APLIKASI************************************************************************/
    private class LogoutTask extends AsyncTask<String, Void, String> {
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Sign Out...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String response = null;
            String res = null;
            try {
                response = CustomHttpClient.executeHttpLogout();
                res=response.toString();
                res= res.replaceAll("\\s+","");
            }
            catch (Exception e) {
                //txt_Error.setText(e.toString());
            }
            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            SharedPreferences sharedpreferences = getActivity().getSharedPreferences(Login.MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.clear();
            editor.commit();

            Intent in = new Intent(getActivity(), Login.class);
            startActivity(in);
            getActivity().finish();
        }//close onPostExecute
    }// close validateUserTask


/***********************************NAMPILIN POSTINGAN************************************************************************/
    private class ShowContentTask extends AsyncTask<String, Void, ArrayList<Destinasi>> {
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("ENTENI WAE CUK!!!");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected ArrayList<Destinasi> doInBackground(String... params) {
            ArrayList<Destinasi> respon = new ArrayList<>();
            try {
                respon = CustomHttpClient.executeHttpGet("http://progmob.esy.es/getUsers.php",spUsername);
            }
            catch (Exception e) {
                //txt_Error.setText(e.toString());
            }
            return  respon;
        }//close doInBackground

        @Override
        protected void onPostExecute(ArrayList<Destinasi> result) {
            pDialog.dismiss();
            dest = result;
            setupFeed(result);

            mSwipeRefreshLayout.setRefreshing(false);
        }//close onPostExecute
    }


    private class UpdateContentTask extends AsyncTask<String, Void, ArrayList<Destinasi>> {
        @Override
        protected ArrayList<Destinasi> doInBackground(String... params) {
            ArrayList<Destinasi> respon = new ArrayList<>();
            try {
                respon = CustomHttpClient.executeHttpGet("http://progmob.esy.es/getUsers.php",spUsername);
            } catch (Exception e) {
                //txt_Error.setText(e.toString());
            }
            return respon;
        }//close doInBackground

        @Override
        protected void onPostExecute(ArrayList<Destinasi> result) {

            dest = result;
            setupFeed(result);

            mSwipeRefreshLayout.setRefreshing(false);
        }//close onPostExecute
    }

}

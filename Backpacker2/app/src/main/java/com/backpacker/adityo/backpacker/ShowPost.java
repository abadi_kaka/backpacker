package com.backpacker.adityo.backpacker;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.backpacker.adityo.backpacker.adapter.CommentAdapter;
import com.backpacker.adityo.backpacker.customclass.Comments;
import com.backpacker.adityo.backpacker.customclass.Destinasi;
import com.backpacker.adityo.backpacker.customclass.Friends;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;


public class ShowPost extends Activity {
    ImageView img;
    TextView loc,uname;
    ListView list;
    CommentAdapter cAdapter;

    ActionBar ab;

    int pos;
    Destinasi temp;
    ArrayList<Comments> comments = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_post);
        img = (ImageView)findViewById(R.id.contentImage);
        loc = (TextView)findViewById(R.id.txtLoc);
        uname = (TextView)findViewById(R.id.txtUname);
        list = (ListView)findViewById(R.id.listView);

        ab = getActionBar();
        ab.setTitle("Backpacker");

        if(getIntent().hasExtra("position")){
            pos = getIntent().getIntExtra("position",0);
            temp = Backpacker.dest.get(pos);
            img.setImageBitmap(temp.getImg());
            loc.setText(temp.getLoc());
            uname.setText(temp.getUsername());

            new ShowComments().execute();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_show_post, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class ShowComments extends AsyncTask<String, Void, ArrayList<Comments>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<Comments> doInBackground(String... params) {
            ArrayList<Comments> respon = new ArrayList<>();
            try {
                respon = CustomHttpClient.executeHttpGetComments("http://progmob.esy.es/comment.php", String.valueOf(temp.getId()));
            }
            catch (Exception e) {
            }
            return  respon;
        }//close doInBackground

        @Override
        protected void onPostExecute(ArrayList<Comments> result) {
            comments = result;
  /*          Comments c = comments.get(0);
            Toast.makeText(ShowPost.this,c.getUsername(),Toast.LENGTH_LONG).show();
*/
            cAdapter = new CommentAdapter(ShowPost.this, R.layout.comment_adapter, comments);
            list.setAdapter(cAdapter);
        }//close onPostExecute
    }

}

package com.backpacker.adityo.backpacker;

import java.util.ArrayList;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Login extends Activity {


    //shared preference
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "BackpackerPref" ;
    public static final String name = "unameKey";
    public static final String pass = "passKey";


    //define controls
    EditText  txt_uname, txt_pwd;
    TextView  txt_Error;
    Button btnLogin;
    Button btnReg;
    String response = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //initalise controls
        txt_uname=(EditText)findViewById(R.id.txtUsername);
        txt_pwd=(EditText)findViewById(R.id.txtPassword);
        btnLogin =(Button)findViewById(R.id.btnLogin);
        txt_Error =(TextView)findViewById(R.id.txtError);
        btnReg = (Button)findViewById(R.id.btnReg);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                String uname = txt_uname.getText().toString();
                String pwd = txt_pwd.getText().toString();

                InputMethodManager imm = (InputMethodManager)getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(txt_uname.getWindowToken(), 0);

                validateUserTask task = new validateUserTask();
                task.execute(new String[]{uname, pwd});
            }
        }); //close on listener

        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this, register.class);
                startActivity(i);
            }
        });
    }// close onCreate
    
    
    @Override
    protected void onResume() {
        sharedpreferences=getSharedPreferences(MyPREFERENCES,
                Context.MODE_PRIVATE);
        if (sharedpreferences.contains(name))
        {
            if(sharedpreferences.contains(pass)){
                Intent i = new Intent(this,Backpacker.class);
                startActivity(i);
                Login.this.finish();
            }
        }
        super.onResume();
    }
    
    
    
    

    private class validateUserTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
            postParameters.add(new BasicNameValuePair("username", params[0] ));
            postParameters.add(new BasicNameValuePair("password", params[1] ));
            String res = null;
            try {
                response = CustomHttpClient.executeHttpPost("http://progmob.esy.es/check.php", postParameters);
                res=response.toString();
                res= res.replaceAll("\\s+","");
            }
            catch (Exception e) {
                txt_Error.setText(e.toString());
            }
            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            if(result.equals("1")){
                //navigate to Main Menu
                SharedPreferences.Editor editor = sharedpreferences.edit();
                String username = txt_uname.getText().toString();
                String passwd = txt_pwd.getText().toString();
                editor.putString(name, username);
                editor.putString(pass, passwd);
                editor.commit();


                Intent i = new Intent(Login.this,Backpacker.class);
                startActivity(i);
                Login.this.finish();
            }
            else{
                txt_Error.setText(result);
                txt_Error.setVisibility(View.VISIBLE);
            }
        }//close onPostExecute
    }// close validateUserTask
}//close LoginActivity
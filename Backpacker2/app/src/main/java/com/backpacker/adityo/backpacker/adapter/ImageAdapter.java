package com.backpacker.adityo.backpacker.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.backpacker.adityo.backpacker.Backpacker;
import com.backpacker.adityo.backpacker.R;
import com.backpacker.adityo.backpacker.customclass.Destinasi;

import java.util.ArrayList;

/**
 * Created by adityo on 5/14/2015.
 */
public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    ArrayList<Destinasi> items = new ArrayList<>();
    public ImageAdapter(Context c, ArrayList<Destinasi> i) {
        mContext = c;
        items = i;
    }

    public int getCount() {
        return items.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(mContext);
        imageView.setImageBitmap(items.get(position).getImg());
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setLayoutParams(new GridView.LayoutParams(235, 235));
        return imageView;
    }

    // references to our images
    private Integer[] mThumbIds = {
            R.drawable.home,
            R.drawable.home,
            R.drawable.home,
            R.drawable.home,
            R.drawable.home
    };
}
